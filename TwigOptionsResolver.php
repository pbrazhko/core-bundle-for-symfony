<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 16:55
 */

namespace CMS\CoreBundle;

use CMS\CoreBundle\Interfaces\TwigOptionsInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class TwigOptionsResolver implements TwigOptionsInterface
{
    protected function setDefined(OptionsResolver $resolver)
    {
        // TODO: Implement setDefined() method.
    }

    protected function setRequired(OptionsResolver $resolver)
    {
        // TODO: Implement setRequired() method.
    }

    protected function setDefaults(OptionsResolver $resolver)
    {
        // TODO: Implement setDefaults() method.
    }

    protected function setAllowedTypes(OptionsResolver $resolver)
    {
        // TODO: Implement setAllowedTypes() method.
    }

}