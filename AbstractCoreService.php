<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 05.11.14
 * Time: 17:04
 */

namespace CMS\CoreBundle;

use CMS\CoreBundle\Interfaces\CoreServiceInterface;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

/**
 * Class AbstractCoreService
 * @package CMS\CoreBundle
 */
abstract class AbstractCoreService implements CoreServiceInterface
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var
     */
    protected $repository;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->getRepository()->findOneBy($criteria, null, null, array(), null, 1, $orderBy);
    }

    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @param Criteria $criteria
     * @return array
     */
    public function findByCriteria(Criteria $criteria)
    {
        return $this->getRepository()->findByCriteria($criteria);
    }

    /**
     * @param $method
     * @param $arguments
     * @throws \Exception
     * @return mixed|null
     */
    public function __call($method, $arguments)
    {
        switch (true) {
            case (0 === strpos($method, 'findBy')):
                $by = substr($method, 6);
                $method = 'findBy';
                break;

            case (0 === strpos($method, 'findOneBy')):
                $by = substr($method, 9);
                $method = 'findOneBy';
                break;

            default:
                throw new \BadMethodCallException(
                    "Undefined method '$method'. The method name must start with " .
                    "either findBy or findOneBy!"
                );
        }

        switch (count($arguments)) {
            case 1:
                return $this->$method(array(strtolower($by) => $arguments[0]));

            case 2:
                return $this->$method(array(strtolower($by) => $arguments[0]), $arguments[1]);

            case 3:
                return $this->$method(array(strtolower($by) => $arguments[0]), $arguments[1], $arguments[2]);

            case 4:
                return $this->$method(array(strtolower($by) => $arguments[0]), $arguments[1], $arguments[2], $arguments[3]);

            default:
                // Do nothing
        }

        throw new \InvalidArgumentException(sprintf('Method \'%s\' is not supported', $method));
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->getRepository()->create($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->getRepository()->delete($id);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function update($data)
    {
        return $this->getRepository()->update($data);
    }

    /**
     * @param null $data
     * @param array $options
     * @return \Symfony\Component\Form\Form
     * @throws \Exception
     */
    public function generateForm($data = null, array $options = [])
    {
        $formFactory = $this->container->get('form.factory');

        $result = $this->configureForm($formFactory, $data, $options);

        if (!$result instanceof FormBuilder) {
            throw new \Exception('not supported response');
        }

        return $result->getForm();
    }

    /**
     * @return string
     */
    public function getRepositoryClass()
    {
        return $this->getRepository()->getClassName();
    }

    /**
     * @return \CMS\CoreBundle\AbstractCoreRepository
     */
    public function getRepository()
    {
        if (null === $this->repository) {
            $this->repository = $this->container->get('doctrine.orm.entity_manager')
                ->getRepository($this->getRepositoryName());
        }

        return $this->repository;
    }
}