<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.10.14
 * Time: 14:11
 */

namespace CMS\CoreBundle;


use CMS\CoreBundle\Interfaces\CoreRepositoryInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Persisters\Entity\EntityPersister;

/**
 * Class AbstractCoreRepository
 * @package CMS\CoreBundle
 */
abstract class AbstractCoreRepository extends EntityRepository implements CoreRepositoryInterface
{

    /**
     * @var null
     */
    private $unitOfWork = null;
    /**
     * @var EntityRepository
     */
    private $repository = null;

    /**
     * Create
     *
     * @param array|object $data
     * @return mixed
     */
    public function create($data)
    {
        $this->_em->persist($data);
        $this->_em->flush();

        return true;
    }

    /**
     * Read data from repository
     *
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     * @return mixed
     */
    public function read(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getPersister()->loadAll($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param Criteria $criteria
     * @return array
     */
    public function readByCriteria(Criteria $criteria)
    {
        return $this->getPersister()->loadCriteria($criteria);
    }

    /**
     * Update
     *
     * @param array|object $data
     * @return mixed
     */
    public function update($data)
    {
        $this->_em->persist($data);
        $this->_em->flush();

        return true;
    }

    /**
     * Delete
     *
     * @param int $id
     * @return mixed
     */
    public function delete($id)
    {
        $persister = $this->getPersister();

        $object = $persister->loadAll(array('id' => $id));

        if (!$object){
            return false;
        }

        return $persister->delete(reset($object));
    }

    /**
     * Check is field exists
     *
     * @param $field
     * @return bool
     */
    public function hasField($field){
        $persister = $this->getPersister();

        $classMetaData = $persister->getClassMetadata();

        if ($classMetaData->hasField($field) || $classMetaData->hasAssociation($field)){
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getFields(){
        $persister = $this->getPersister();

        $classMetaData = $persister->getClassMetadata();

        return $classMetaData->getFieldNames();
    }

    /**
     * @param $field
     * @return array
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function getFieldMapping($field){
        $persister = $this->getPersister();

        $classMetaData = $persister->getClassMetadata();

        return $classMetaData->getFieldMapping($field);
    }

    /**
     * @return EntityPersister
     */
    private function getPersister()
    {
        return $this->getUnitOfWork()->getEntityPersister($this->getRepositoryName());
    }

    /**
     * @return \Doctrine\ORM\UnitOfWork|null
     */
    private function getUnitOfWork()
    {
        if(null === $this->unitOfWork){
            $this->unitOfWork = $this->getEntityManager()->getUnitOfWork();
        }

        return $this->unitOfWork;
    }

    /**
     * @return EntityRepository
     */
    private function getRepository()
    {
        if (null === $this->repository) {
            $this->repository = $this->getEntityManager()->getRepository($this->getRepositoryName());
        }

        return $this->repository;
    }
} 