<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 14.10.15
 * Time: 13:27
 */

namespace CMS\CoreBundle\Listener;


use CMS\UsersBundle\Entity\Users;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class EntityModifierListener
{

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var bool
     */
    private $isCLI = false;

    /**
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
        $this->isCLI = ( php_sapi_name() == 'cli' );
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        if($this->isCLI){
            return;
        }

        $entity = $args->getEntity();

        if (method_exists($entity, 'setDateUpdate')) {
            $entity->setDateUpdate(new \DateTime());
        }

        $user = $this->tokenStorage->getToken()->getUser();

        if ($user instanceof Users && method_exists($entity, 'setUpdateBy')) {
            $entity->setUpdateBy($user->getId());
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        if($this->isCLI){
            return;
        }

        $entity = $args->getEntity();

        if (method_exists($entity, 'setDateCreate')) {
            $entity->setDateCreate(new \DateTime());
        }

        if (method_exists($entity, 'setDateUpdate')) {
            $entity->setDateUpdate(new \DateTime());
        }

        $user = $this->tokenStorage->getToken()->getUser();

        if ($user instanceof Users && method_exists($entity, 'setCreateBy')) {
            $entity->setCreateBy($user->getId());
        }

        if ($user instanceof Users && method_exists($entity, 'setUpdateBy')) {
            $entity->setUpdateBy($user->getId());
        }
    }
}