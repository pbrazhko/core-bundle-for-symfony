<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 17.09.15
 * Time: 17:38
 */

namespace CMS\CoreBundle\Listener;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AjaxAuthenticationListener
{
    public function onCoreException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $request = $event->getRequest();

        if ($request->isXmlHttpRequest() && $exception instanceof AccessDeniedException) {
            $event->setResponse(new JsonResponse(['status' => 'fail', 'error' => 'Access denied!'], 403));
        }
    }
}