<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.01.15
 * Time: 22:31
 */

namespace CMS\CoreBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class TransliterService
{
    private $container;

    private $converter = array(
        'а' => 'a', 'б' => 'b', 'в' => 'v',
        'г' => 'g', 'д' => 'd', 'е' => 'e',
        'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
        'и' => 'i', 'й' => 'y', 'к' => 'k',
        'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u',
        'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
        'ь' => "'", 'ы' => 'y', 'ъ' => "'",
        'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

        'А' => 'A', 'Б' => 'B', 'В' => 'V',
        'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
        'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
        'И' => 'I', 'Й' => 'Y', 'К' => 'K',
        'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U',
        'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
        'Ь' => "'", 'Ы' => 'Y', 'Ъ' => "'",
        'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',

        'ь' => '', 'Ь' => '', 'ъ' => '',
        'Ъ' => '', ' ' => '_', '\'' => '',
        '"' => '', '(' => '', ')' => '',
        ',' => '', '.' => '', '!' => '',
        '?' => '', '%' => '', '\\' => '',
        '/' => '', '$' => '', '@' => ''
    );

    private $case = false;
    private $case_direction = 'down';

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function translit($str)
    {
        if (null === $str || empty($str)) {
            return null;
        }

        if ($this->case) {
            $str = ($this->case_direction == 'up') ? mb_strtoupper($str, $this->container->getParameter('kernel.charset')) : mb_strtolower($str, $this->container->getParameter('kernel.charset'));
        }

        return strtr($str, $this->converter);
    }

    public function addToConverter($key, $value)
    {
        $this->converter = array_merge($this->converter, array($key, $value));

        return $this;
    }

    public function addToCoverterArray(array $coverter)
    {
        $this->converter = array_merge($this->converter, $coverter);

        return $this;
    }

    public function removeFromConverter($key)
    {
        if (array_key_exists($key, $this->converter)) {
            unset($this->converter[$key]);
        }

        return $this;
    }

    public function setCase($case)
    {
        if (in_array($case, array('up', 'down'))) {
            $this->case = true;
            $this->case_direction = $case;
        }

        return $this;
    }

    public function getCase()
    {
        return $this->case;
    }

    public function getCaseDirection()
    {
        return $this->case_direction;
    }
} 