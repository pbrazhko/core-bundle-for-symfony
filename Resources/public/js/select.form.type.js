/**
 * Created by work-pc on 02.07.16.
 */

(
    function($){
        var defaultConfig = {
            'find_route': null,
            'name': 'typeahead',
            'locale': null,
            'typeahead_template': '<div>{{value}}</div>',
            'item_template': '<div>{{title}}</div>',
            'items_container': null
        };

        var CmsSelectFormType = {
            config: {},
            selector: null,
            itemsPrototype: null,
            init: function(selector, config){
                if(!this.checkRequirements){
                    return false;
                }

                this.selector = selector;
                this.config = $.extend({}, defaultConfig, config);

                if(this.config.find_route == undefined || this.config.find_route == ''){
                    console.error('Find route is not defined!');
                    return;
                }

                this.bind();
            },
            checkRequirements: function(){
                if(!$.isFunction($.fn.typeahead)){
                    console.error('Typeahead is not defined!');
                    return false;
                }

                if(!$.isFunction(Routing)){
                    console.error('Routing is not defined!');
                    return false;
                }

                return true;
            },
            bind: function(){
                $(this.selector)
                    .typeahead(
                        {
                            hint: true,
                            highlight: true
                        },
                        {
                            'name': this.config.name,
                            'templates': {
                                'suggestion': Handlebars.compile(this.config.typeahead_template)
                            },
                            'display': 'title',
                            'limit': 9999999999999,
                            'source': this.getBloodhound()
                        }
                    )
                    .bind('typeahead:select', this.selectSuggestion)
                    .bind('typeahead:asyncreceive', function (a, b, c, d){
                        console.log('s');
                    });

                $('.cms-select-form-type-delete').click(this.deleteRow);
            },
            selectSuggestion: function(ev, suggestion){
                $(this).typeahead('val', '');
                CmsSelectFormType.addItem(suggestion);
            },
            addItem: function(data){
                var compiler = Handlebars.compile(this.getItemsPrototype());
                var htmlPrototype = $.parseHTML(compiler(data));

                $(htmlPrototype).find('.cms-select-form-type-delete').click(this.deleteRow);
                                
                $(this.config.items_container).append(htmlPrototype);
            },
            deleteRow:function(e){
                e.preventDefault();

                $(this).closest('.cms-select-form-type-row').remove();
            },
            getItemsCount: function(){
                return $(this.config.items_container).find(':input').length;
            },
            getItemsPrototype: function(){
                return this.config.item_template;
            },
            getBloodhound: function(){
                return new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: Routing.generate(
                            this.config.find_route,
                            {
                                'query': '--QUERY--'
                            }
                            
                        ),
                        wildcard: '--QUERY--'
                    }
                });
            }
        };

        $.fn.cmsSelectFormType = function (config){
            return CmsSelectFormType.init(this, config);
        }
    }
)(jQuery);