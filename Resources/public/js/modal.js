(
    function($){
        $.modal = function(config){
            return $.modal.impl.init(config);
        };

        $.modal.config = {
            'contentType': 'html',
            'content': '',
            'showClose': true,
            'showHeader': true,
            'showFooter': true,
            'title': 'Modal',
            'footer': '',
            'width': 'auto',
            'closeImage': '/img/close.png'
        };

        $.modal.impl = {
            config: {},
            modal: null,
            shadow: null,
            allowedTypes: ['html', 'text', 'iframe'],
            init: function(config){
                this.config = $.extend({}, $.modal.config, config);

                if($.inArray(this.config.contentType, this.allowedTypes) < 0){
                    console.log('Not supported content type!');
                    return;
                }

                this.open();
            },
            open: function(){
                $(document).find('body').append(this.createShadow());
                $(document).find('body').append(this.createModal());

                var windowSize = this.getWindowSize();

                var modalWidth =  this.config.width == 'auto'? $(this.modal).width():this.config.width;

                $(this.modal).attr({
                    'style': 'left: ' + ((windowSize[0] - modalWidth)/2) + 'px;' +
                    'top:' + ((windowSize[1] - $(this.modal).height())/2) + 'px;' +
                    'width:' + modalWidth + 'px;'
                });
            },
            close: function(event){
                $($.modal.impl.modal).remove();
                $($.modal.impl.shadow).remove();
            },
            getPageHeight: function(){
                return Math.max(document.compatMode != 'CSS1Compat' ? document.body.scrollHeight : document.documentElement.scrollHeight, this.getViewportHeight());
            },
            getViewportHeight: function() {
                var ua = navigator.userAgent.toLowerCase();
                var isOpera = (ua.indexOf('opera')  > -1);
                var isIE = (!isOpera && ua.indexOf('msie') > -1);

                return ((document.compatMode || isIE) && !isOpera) ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight;
            },
            getWindowSize: function(){
                var winW, winH = 0;
                if (document.body && document.body.offsetWidth) {
                    winW = document.body.offsetWidth;
                    winH = document.body.offsetHeight;
                }
                if (document.compatMode=='CSS1Compat' &&
                    document.documentElement &&
                    document.documentElement.offsetWidth ) {
                    winW = document.documentElement.offsetWidth;
                    winH = document.documentElement.offsetHeight;
                }
                if (window.innerWidth && window.innerHeight) {
                    winW = window.innerWidth;
                    winH = window.innerHeight;
                }

                return [winW, winH];
            },
            createShadow: function(){
                return this.shadow = $('<div/>')
                    .attr({
                        'class': 'modal-shadow',
                        'style': 'height:' + (this.getPageHeight()-96) + 'px;'
                    });

            },
            createModal: function(){
                this.modal = $('<div/>').attr({'class': 'modal'});
                if(this.config.showHeader){
                    var header = $('<div/>')
                        .attr({'class': 'header'})
                        .html(this.config.title);

                    if(this.config.showClose) {
                        var control = $('<div/>').addClass('close').append($('<img/>')
                            .attr({'src': this.config.closeImage})
                            .on('click', this.close));
                        header.append(control);
                    }

                    this.modal.append(header);
                }

                this.modal.append(
                    $('<div/>')
                        .attr({'class': 'content'})
                        .html(this.buildContent(this.config.content, this.config.contentType))
                );

                if(this.config.showFooter){
                    var footer = $('<div/>')
                        .attr({'class': 'footer'})
                        .html(this.config.footer);
                    this.modal.append(footer);
                }

                return this.modal;
            },
            buildContent: function(content, type){
                switch (type){
                    case 'html':
                        return content;
                    case 'text':
                        return $(content).text();
                    case 'iframe':
                        return $('<iframe/>').attr('src', content);
                    default :
                        return null;
                }
            }
        };
    }
)(jQuery);