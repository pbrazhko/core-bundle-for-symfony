(
    function ($){
        var defaultConfig = {
            'url': null,
            'byClick': false,
            'scrollTo': null,
            'count': 0,
            'offset': 0,
            'limit': 20,
            'onInit': null,
            'preLoad': null,
            'postLoad': null
        };

        var ajaxPaginator = {
            config: {},
            sending: false,
            informer: null,
            counter: null,
            init: function(config){
                this.config = $.extend({}, defaultConfig, config);

                if(!this.validateConfig()){
                    throw Error('Config is not valid!');
                }

                this.bind();

                if($.isFunction(this.config.onInit)){
                    this.config.onInit(this);
                }

                return this;
            },
            validateConfig: function (){
                if(this.config.url === null) return false;
                if(this.config.scrollTo === null) return false;

                return true;
            },
            onScroll: function(){
                if(ajaxPaginator.isScrolledTo() && ajaxPaginator.existsMore() && ajaxPaginator.sending === false) {
                    ajaxPaginator.send();
                }
            },
            onClick: function(){
                if(ajaxPaginator.existsMore() && ajaxPaginator.sending === false) {
                    ajaxPaginator.send();
                }
            },
            isScrolledTo: function(){
                var scrollToHeight = $(this.config.scrollTo).offset().top + $(this.config.scrollTo).outerHeight();

                return this.getWindowScroll() >= scrollToHeight - window.innerHeight
            },
            getWindowScroll: function(){
                return $(window).scrollTop();
            },
            existsMore: function (){
                return this.config.count > this.config.offset;
            },
            getMoreCount: function (){
                return this.config.count - this.config.offset;
            },
            send: function(){
                this.sending = true;

                if($.isFunction(this.config.preLoad)){
                    this.config.preLoad(this);
                }

                var url = this.getUrl();

                $.ajax({
                    'method': 'POST',
                    'url': url,
                    'success': function (result){
                        ajaxPaginator.offsetUp(ajaxPaginator.config.limit);
                        ajaxPaginator.sending = false;

                        window.history.pushState('', $('title').text(), url);

                        if($.isFunction(ajaxPaginator.config.postLoad)){
                            ajaxPaginator.config.postLoad(ajaxPaginator, result);
                        }
                    }
                })
            },
            getUrl: function(){
                var url = this.config.url;

                if($.isFunction(this.config.url)){
                    url = this.config.url(this);
                }

                return url;
            },
            offsetUp: function (count){
                this.config.offset = this.config.offset + count;
            },
            bind: function (){
                if(!this.config.byClick){
                    $(window).scroll(this.onScroll);
                }
            }
        };

        $.ajaxPaginator = function (config){
            return ajaxPaginator.init(config);
        }
    }

)(jQuery);