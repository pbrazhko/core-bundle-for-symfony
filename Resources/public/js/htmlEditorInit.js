/**
 * Created by pavel on 21.05.15.
 */

(function ($) {
    $.htmlEditor = function () {
        return $.htmlEditor.impl.init();
    };

    $.htmlEditor.impl = {
        icons: null,
        init: function () {
            this.createIcons();
            this.bind();
        },
        createIcons: function () {
            this.icons = $('<div/>')
                .addClass('htmlEditor')
                .append($('<span/>')
                    .attr({'class': 'glyphicon glyphicon-pencil'})
            )
                .append('Edit in tinyMCE');
        },
        bind: function () {
            $('textarea').mouseover(function () {
                $(this).append($.htmlEditor.impl.icons);
            });

            $('textarea').mouseout(function () {
                $(this).find('div:last').remove();
            });
        }
    }
})(jQuery)

$(document).ready(function () {
    $.htmlEditor();
})