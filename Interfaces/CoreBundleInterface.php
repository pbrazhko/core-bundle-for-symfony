<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.06.15
 * Time: 17:32
 */

namespace CMS\CoreBundle\Interfaces;


interface CoreBundleInterface
{
    /**
     * @return boolean
     */
    public function isEnabled();

    /**
     * @return array
     */
    public function getDescription();
}