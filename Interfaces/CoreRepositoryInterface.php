<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.10.14
 * Time: 14:08
 */

namespace CMS\CoreBundle\Interfaces;


interface CoreRepositoryInterface {

    /**
     * Create
     *
     * @param array|object $data
     * @return mixed
     */
    public function create($data);

    /**
     * Read data from repository
     *
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     * @return mixed
     */
    public function read(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * Update
     *
     * @param array|object $data
     * @return mixed
     */
    public function update($data);

    /**
     * Delete
     *
     * @param int $id
     * @return mixed
     */
    public function delete($id);

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName();
} 