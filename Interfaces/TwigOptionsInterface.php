<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 16:49
 */

namespace CMS\CoreBundle\Interfaces;

interface TwigOptionsInterface
{
    static function resolve(array $options = array());
    }