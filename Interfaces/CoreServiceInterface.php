<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 05.11.14
 * Time: 17:03
 */

namespace CMS\CoreBundle\Interfaces;


use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

interface CoreServiceInterface {

    /**
     * @return array
     */
    public function getDefaultsCriteria();

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null);

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName();
}