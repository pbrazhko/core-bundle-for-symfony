<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 20.07.16
 * Time: 20:39
 */

namespace CMS\CoreBundle\Twig;


use Symfony\Component\PropertyAccess\PropertyAccess;

class CoreExtension extends \Twig_Extension
{
    /**
     * @var PropertyAccess
     */
    private $propertyAccessor;

    /**
     * CoreExtension constructor.
     */
    public function __construct()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('propertyAccess', [$this, 'propertyAccess'])
        ];
    }

    public function propertyAccess($object, $path)
    {
        if($this->propertyAccessor->isReadable($object, $path)){
            return $this->propertyAccessor->getValue($object, $path);
        }

        return null;
    }

    public function getName()
    {
        return 'core_extension';
    }
}