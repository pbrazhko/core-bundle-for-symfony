<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 16.07.16
 * Time: 20:20
 */

namespace CMS\CoreBundle\Form\Loader;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Form\ChoiceList\EntityLoaderInterface;
use Symfony\Bridge\Doctrine\Form\ChoiceList\IdReader;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;
use Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface;
use Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface;

class ChoiceLoader implements ChoiceLoaderInterface
{
    /**
     * @var ChoiceListFactoryInterface
     */
    private $factory;
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var
     */
    private $class;
    /**
     * @var IdReader
     */
    private $idReader;
    /**
     * @var EntityLoaderInterface
     */
    private $objectLoader;

    private $choiceList;

    /**
     * ChoiceLoader constructor.
     * @param ChoiceListFactoryInterface $factory
     * @param ObjectManager $manager
     * @param $class
     * @param IdReader|null $idReader
     * @param EntityLoaderInterface|null $objectLoader
     */
    public function __construct(ChoiceListFactoryInterface $factory, ObjectManager $manager, $class, IdReader $idReader = null, EntityLoaderInterface $objectLoader = null)
    {
        $classMetadata = $manager->getClassMetadata($class);

        $this->factory = $factory;
        $this->manager = $manager;
        $this->class = $classMetadata->getName();
        $this->idReader = $idReader ?: new IdReader($manager, $classMetadata);
        $this->objectLoader = $objectLoader;
    }

    /**
     * @param null $value
     * @return ArrayChoiceList
     */
    public function loadChoiceList($value = null)
    {
        if ($this->choiceList) {
            return $this->choiceList;
        }

        $objects = [];

        $this->choiceList = $this->factory->createListFromChoices($objects, $value);

        return $this->choiceList;
    }

    /**
     * @param array $values
     * @param null $value
     * @return array|void
     */
    public function loadChoicesForValues(array $values, $value = null)
    {
        return $this->objectLoader->getEntitiesByIds($this->idReader->getIdField(), $values);
    }

    /**
     * @param array $choices
     * @param null $value
     * @return \string[]|void
     */
    public function loadValuesForChoices(array $choices, $value = null)
    {
        $this->choiceList = $this->factory->createListFromChoices($choices, $value);
    }

}